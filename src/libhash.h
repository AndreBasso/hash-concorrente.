#ifndef LIBHASH_H
#define	LIBHASH_H

#include <stdio.h>
#include <stdlib.h>

#define fatorCargaMAX 0.9

int *vetor;
int totalInsercoes;
int tamanhoTabela;
int numeroBlocos;

int funcHash(int m, int n);
void construtor(int m, int n);
void add(int x);
void set(int x);
int busca(int posicao, int *vetorAux);
int get(int posicao);
void dobraTabela();
void deletee(int x);
void destructor();
void print(int x);
void printall();


#endif	/* LIBHASH_H */

