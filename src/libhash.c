#include <stdio.h>
#include <stdlib.h>
#include "libhash.h"

int funcHash(int m, int n) {
    return m % n;
}

void construtor(int m, int n) {

    tamanhoTabela = m;
    numeroBlocos = n;
    
    if ((m % n) == 0) {
        vetor = (int *) malloc(m * sizeof (int));
    }

}

void add(int x) {

    int posicao, fatorCarga;

    posicao = funcHash(x, tamanhoTabela);
    if (vetor[posicao] != 0) {
        posicao = busca(posicao, vetor);
    }

    vetor[posicao] = x;
    totalInsercoes++;
    fatorCarga = totalInsercoes / tamanhoTabela;

    if (fatorCargaMAX < fatorCarga) {
        dobraTabela();
    }

}

void set(int x) {

    int posicao;

    posicao = funcHash(x, tamanhoTabela);

    vetor[posicao] = x;
    totalInsercoes++;

}

int busca(int posicao, int *vetorAux) {
    int livre = 0, posfinal = tamanhoTabela - 1;

    posicao = posicao + 1; //como a posicao atual ja esta ocupada, vai pra proxima
    while (livre == 0) { //varre o vetor ate achar uma posicao livre
        if (vetorAux[posicao] == 0) { //se a posicao esta disponivel
            livre = 1; //condicao de parada
        } else {
            if (posicao == posfinal) { //se chegar na ultima posicao do vetor, precisa comecar na inicial
                posicao = 0;
            } else {
                posicao = posicao + 1; //pula para a proxima posicao
            }
        }
    }

    return posicao; //retorna posicao disponivel
}

int get(int posicao) {
    return vetor[posicao];
}

void dobraTabela(void) { //funcao que dobra a tabela e realoca os valores
    int i, n, posicao;
    int *vetorAux;

    n = tamanhoTabela;
    tamanhoTabela *= 2; //dobra o tamanho da Tabela
    vetorAux = (int*) malloc(tamanhoTabela * sizeof (int));

    for (i = 0; i < n; i++) { //copia para o novo vetor
        if (vetor[i] != 0) { //se for diferente de 0, ocorre colisao
            posicao = funcHash(vetor[i], n);
            if (vetorAux[posicao] != 0) {
                posicao = busca(posicao, vetorAux);
            }
            vetorAux[posicao] = vetor[i];
        }
    }

    free(vetor);
    vetor = vetorAux;
}

void deletee(int x) {
    int posicao;
    
    posicao = funcHash(x, tamanhoTabela);
    vetor[posicao] = 0;
}

void destructor() {
    free(vetor);
}

void print(int x) {
    int posicao;
    
    posicao = funcHash(x, tamanhoTabela);
    
    if(vetor[posicao] != 0) {
        printf("%d\n", vetor[posicao]);
    } else {
        printf("NULL\n");
    }
    
}

void printall() {
    int posicao;
    
    for(posicao = 0; posicao < tamanhoTabela; posicao++){
        if(vetor[posicao] != 0) {
                printf("%d ", vetor[posicao]);
        } else {
                printf("NULL ");
        }
    }
    printf("\n");
}
